<?php

/*
 * MIT License
 *
 * Copyright (c) 2022 Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace asmaru\serializer;

use InvalidArgumentException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 * @covers \asmaru\serializer\Serializer
 */
class SerializerTest extends TestCase {

	public function testSerialize() {
		$expected = '{"string":"string","int":1,"bool":true,"modelB":{"float":1.1}}';
		$serializer = new Serializer();
		$result = $serializer->serialize($this->createTestObject());
		Assert::assertEquals($expected, $result);
	}

	/**
	 * @return void
	 * @throws ReflectionException
	 */
	public function testDeserialize() {
		$expected = $this->createTestObject();
		$serializedObject = '{"string":"string","int":1,"bool":true,"modelB":{"float":1.1}}';
		$serializer = new Serializer();
		$result = $serializer->deserialize($serializedObject, ModelA::class);
		Assert::assertEquals($expected, $result);
	}

	/**
	 * @return void
	 * @throws ReflectionException
	 */
	public function testDeserializeInvalidClass() {
		$serializedObject = '';
		$serializer = new Serializer();
		$this->expectException(InvalidArgumentException::class);
		$serializer->deserialize($serializedObject, SerializerTest::class);
	}

	private function createTestObject(): ModelA {

		$modelB = new ModelB();
		$modelB->setFloat(1.1);

		$modelA = new ModelA();
		$modelA->setString('string');
		$modelA->setInt(1);
		$modelA->setBool(true);
		$modelA->setModelB($modelB);

		return $modelA;
	}
}
