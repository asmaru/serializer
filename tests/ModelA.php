<?php

/*
 * MIT License
 *
 * Copyright (c) 2022 Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace asmaru\serializer;

use JsonSerializable;
use function boolval;
use function intval;
use function strval;

class ModelA implements JsonSerializable, JsonDeserializable {

	private string $string;

	private int $int;

	private bool $bool;

	private ModelB $modelB;

	public function getString(): string {
		return $this->string;
	}

	public function setString(string $string): void {
		$this->string = $string;
	}

	public function getInt(): int {
		return $this->int;
	}

	public function setInt(int $int): void {
		$this->int = $int;
	}

	public function isBool(): bool {
		return $this->bool;
	}

	public function setBool(bool $bool): void {
		$this->bool = $bool;
	}

	public function getModelB(): ModelB {
		return $this->modelB;
	}

	public function setModelB(ModelB $modelB): void {
		$this->modelB = $modelB;
	}

	public static function jsonDeserialize(array $data): static {
		$instance = new static();
		$instance->string = strval($data['string']);
		$instance->int = intval($data['int']);
		$instance->bool = boolval($data['string']);
		$instance->modelB = ModelB::jsonDeserialize($data['modelB']);
		return $instance;
	}

	public function jsonSerialize(): array {
		return [
			'string' => $this->string,
			'int' => $this->int,
			'bool' => $this->bool,
			'modelB' => $this->modelB->jsonSerialize()
		];
	}
}