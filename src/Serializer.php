<?php

/*
 * MIT License
 *
 * Copyright (c) 2022 Stephan Reifsteck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace asmaru\serializer;

use InvalidArgumentException;
use JsonSerializable;
use ReflectionClass;
use ReflectionException;
use function is_string;
use function json_decode;
use function json_encode;

class Serializer {

	/**
	 * Converts an array or JSON string into an instance of the specified class.
	 *
	 * @param mixed $data
	 * @param string $class
	 *
	 * @return mixed
	 * @throws ReflectionException
	 */
	public function deserialize(mixed $data, string $class): mixed {
		$data = is_string($data) ? json_decode($data, true) : $data;
		$reflectionClass = new ReflectionClass($class);
		if (!$reflectionClass->implementsInterface(JsonDeserializable::class)) {
			throw new InvalidArgumentException(sprintf('Class "%s" must implement the %s interface!', $class, JsonSerializable::class));
		}
		return $reflectionClass->getMethod('jsonDeserialize')->invoke(null, $data);
	}

	/**
	 * @param JsonSerializable $object
	 *
	 * @return bool|string
	 */
	public function serialize(JsonSerializable $object): bool|string {
		return json_encode($object);
	}
}